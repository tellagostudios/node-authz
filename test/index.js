var assert = require('assert'),
    parser = require('../lib/authz-parser.js'),
    authz  = require('..');

describe('authz', function () {

    describe('parser', function () {

        it('should parse a simple statement using allow as default', function () {

            var ast = parser.parse('get foo');

            assert.ok(ast);
            assert.equal(ast.length, 1);
            assert.equal(ast[0].action, 'GET');
            assert.equal(ast[0].type, 'allow');
        });

        it('should parse a multiline statement', function () {

            var ast = parser.parse('get foo\nget bar');

            assert.ok(ast);
            assert.equal(ast.length, 2);
        });

        it('should map alias to right actions', function () {

            var get = ["get", "query", "fetch", "find", "read"],
                post = ["post", "create", "insert", "send"],
                put = ["put", "update", "modify"],
                del = ["delete", "del", "destroy", "pop", "drop"];

            for ( var i in get ) {
                var ast = parser.parse(get[i] + ' foo');
                assert.equal(ast[0].action, 'GET');
            }

            for ( var i in post ) {
                var ast = parser.parse(post[i] + ' foo');
                assert.equal(ast[0].action, 'POST', post[i]);
            }

            for ( var i in put ) {
                var ast = parser.parse(put[i] + ' foo');
                assert.equal(ast[0].action, 'PUT');
            }

            for ( var i in del ) {
                var ast = parser.parse(del[i] + ' foo');
                assert.equal(ast[0].action, 'DELETE');
            }
        });
    });
    
    describe('validator', function () {

        it('should match a simple url', function () {

            var check = authz('get foo');

            assert.ok(check('/foo'));
        });

        it('should not match an invalid simple url', function () {

            var check = authz('get foo');

            assert.ok(!check('/bar'));
        });

        it('should match a wildcard url', function () {

            var check = authz('get foo.*');

            assert.ok(check('/foo/bar'));
        });

        it('should match a sub folder of a wildcard', function () {

            var check = authz('get foo.*');

            assert.ok(check('/foo/bar/baz'));
        });

        it('should allow subfolders after a wildcard', function () {

            var check = authz('allow read foo.*.baz');

            assert.ok(check('/foo/bar/baz'));
        });

        it('should deny in subfolders', function () {

            /*
            [5/27/13 1:57:05 PM] Leandro Boffi: elijo deny by default
            [5/27/13 1:57:13 PM] Leandro Boffi: y despues le doy
            [5/27/13 1:57:38 PM] Leandro Boffi: 
            allow: * /storage/*
            deny: * /storage/sales/*
            */

            var check = authz('allow all storage.*\ndeny all storage.sales');

            assert.ok(check('/storage/orders'));
            assert.ok(!check('/storage/sales/foo'));
        });
    });
});