var parser = require('./lib/authz-parser.js');

module.exports = function ( config ) {

	var rules = parser.parse(config) || [];

	return function (method, url) {

		var allowed = false;

		for(var i in rules) {

			var rule = rules[i];

			if (!rule || !rule.resource) continue;

			/**
			 * do not evaluate further 'allow' rules if it has already
			 * been allowed.
			 */
			if ((!allowed && rule.type === 'allow')) {

				var hasUrl = url.match(rule.resource);
				var hasMethod = rule.action == "*" || rule.action.toUpperCase() == method.toUpperCase();

				allowed = hasMethod && hasUrl;

			} else if (rule.type === 'deny') {

				/**
				 * when a url is denied, inmediately deny access to the
				 * url.
				 */

				var hasUrl = url.match(rule.resource)
				var hasMethod = rule.action == "*" || rule.action.toUpperCase() == method.toUpperCase();

				if (hasUrl && hasMethod) return false;
			}
		}
		
		return allowed;
	};
};