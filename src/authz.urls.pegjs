start
  = rules

space = " "

word = letters:[a-z]+ { return letters.join(''); }

part = w:(word) { return '/' + w; } / any { return '/([a-zA-Z0-9/]*)' } / '...' { return '/([a-zA-Z0-9]*)'; }

resource = parts:(part ( '.' part)?)+ { return new RegExp(parts.join('')) }

create = ("POST" / "CREATE" / "INSERT" / "SEND") { return "POST" }

query = ("GET" / "QUERY" / "FETCH" / "FIND" / "READ") { return "GET" }

update = ("PUT" / "UPDATE" / "MODIFY") { return "PUT" }

delete = ("DELETE" / "DEL" / "DESTROY" / "POP" / "DROP") { return "DELETE" }

any = ("*" / "any" / "all") { return '*' }
action = query / create / update / delete / any

type = ("deny" / "-") { return "deny" }/ ("allow" / "+") { return "allow" }

rule = t:(t:type space { return t })? a:action space r:resource eol? {return { action: a, resource :r, type: t || 'allow' } }

eol = '\n'

rules = rule+

