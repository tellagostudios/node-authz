start
  = rules

space = " "

word = letters:[a-z]+ { return letters.join(''); }

separator = '.'

part = p:(w:(word) / any { return '([a-zA-Z0-9/]*)' }) separator? {return p }

resource = parts:part+ { return new RegExp('/' + parts.join('/')) }

create = ("post" / "create" / "insert" / "send") { return "POST" }

query = ("get" / "query" / "fetch" / "find" / "read") { return "GET" }

update = ("put" / "update" / "modify") { return "PUT" }

delete = ("delete" / "del" / "destroy" / "pop" / "drop") { return "DELETE" }

any = ("*" / "any" / "all") { return '*' }
action = query / create / update / delete / any

type = ("deny" / "-") { return "deny" }/ ("allow" / "+") { return "allow" }

rule = t:(t:type space { return t })? a:action space r:resource tag:word? eol? {return { action: a, resource :r, type: t || 'allow', tag: tag } }

eol = '\n'

rules = rule+
